<?php

/**
 * Implementation of hook_node_info().
 */
function exact_date_filters_example_node_info() {
  $items = array(
    'date_filter_test' => array(
      'name' => t('Date Filter Test'),
      'module' => 'features',
      'description' => t('This is to test date filter'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function exact_date_filters_example_views_api() {
  return array(
    'api' => '2',
  );
}
